#include <mpi.h>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    MPI_Init(&argc, &argv);

    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (world_rank == 0) {
        // Этот процесс будет посылать сообщение
        int buf = 1337;
        MPI_Send(&buf, 1, MPI_INTEGER, 1, 1, MPI_COMM_WORLD);
        cout << "sent " << buf << endl;
    } else if (world_rank == 1) {
        // Этот процесс будет принимать сообщение
        int buf;
        MPI_Status status;
        MPI_Recv(&buf, 1, MPI_INTEGER, 0, 1, MPI_COMM_WORLD, &status);
        cout << "received " << buf << endl;
    }

    MPI_Finalize();

    return 0;
}