#include <mpi.h>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    MPI_Init(&argc, &argv);

    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if (world_rank == 0) {
        // Этот процесс будет посылать сообщение
        uint8_t buf[] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        MPI_Send(&buf, sizeof(buf), MPI_BYTE, 1, 1, MPI_COMM_WORLD);
        cout << "sent" << endl;
    } else if (world_rank == 1) {
        // Этот процесс будет принимать сообщение
        uint8_t buf[8];
        int count;
        MPI_Status status;
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_BYTE, &count);
        if (sizeof(buf) >= count) {
            MPI_Recv(&buf, sizeof(buf), MPI_BYTE, status.MPI_SOURCE, status.MPI_TAG, MPI_COMM_WORLD, nullptr);
            cout << "received" << endl;
        } else {
            cout << "message too big" << endl;
        }
    }

    MPI_Finalize();

    return 0;
}