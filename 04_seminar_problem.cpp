#include <mpi.h>
#include <iostream>
#include <cstdlib>
using namespace std;

#define DEBUG 0

int main(int argc, char** argv) {

    MPI_Init(&argc, &argv);

    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    const int MY_TAG = 0;
    int val;

    if (world_rank == 0) {
        // Первый процесс генерирующий некое случайное число
        val = rand() % 10;
        MPI_Send(&val, 1, MPI_INTEGER, world_rank+1, MY_TAG, MPI_COMM_WORLD);
        cout << "rank " << world_rank << " sent " << val << endl;
    } else if (world_rank == world_size - 1) {
        // Последний процесс, который только выводит число
        MPI_Recv(&val, 1, MPI_INTEGER, world_rank-1, MY_TAG, MPI_COMM_WORLD, nullptr);
        cout << "rank " << world_rank << " recv " << val << endl;
    } else {
        // Остальные процессы, которые получают число, увеличивают его и посылают следующему
        MPI_Recv(&val, 1, MPI_INTEGER, world_rank - 1, MY_TAG, MPI_COMM_WORLD, nullptr);
#if DEBUG
        cout << "rank " << world_rank << " recv " << val << endl;
#endif
        val += world_rank;
        MPI_Send(&val, 1, MPI_INTEGER, world_rank+1, MY_TAG, MPI_COMM_WORLD);
#if DEBUG
        cout << "rank " << world_rank << " sent " << val << endl;
#endif
    }

    MPI_Finalize();

    return 0;
}