#include <mpi.h>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    MPI_Init(&argc, &argv);

    int world_size, world_rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Deadlock alert !!!

    if (world_rank == 0 || world_rank == 1) {
        uint8_t buf[8192] = {};

        cout << "before send " << world_rank << endl;

        // Блокирующий send
        MPI_Send(&buf, sizeof(buf), MPI_BYTE, !world_rank, 1, MPI_COMM_WORLD);
        cout << "sent " << world_rank << endl;

        cout << "before recv " << world_rank << endl;

        // Блокирующий recv
        MPI_Status status;
        MPI_Recv(&buf, sizeof(buf), MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        cout << "after recv " << world_rank << endl;
    }

    MPI_Finalize();

    return 0;
}