#include <mpi.h>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {

    // Инициализация окружения
    MPI_Init(&argc, &argv);

    // Пролучение версии MPI
    int major, minor;
    MPI_Get_version(&major, &minor);

    // Получение числа процессов
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Получение идентификатора текущего процесса
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Получение имени обработчика (обычно hostname)
    int name_length;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_length);

    // Вывод полученных данных
    cout << "Hello from processor " << processor_name << " rank " << world_rank << " out of " << world_size << " MPI v" << major << "." << minor << endl;

    // Уничтожение окружения
    MPI_Finalize();

    return 0;
}